import React, { Component } from "react";
import TodoItems from "./TodoItems";
import "./TodoList.css";

class TodoList extends Component {
    constructor(props){
        super(props);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);

    }

    addItem(e) {//Funcion para agregar un nuevo objeto a la lista
        if (this._inputElement.value !== "") {
          var newItem = {
            text: this._inputElement.value,
            key: Date.now()
          };
       
          this.setState((prevState) => {
            return { 
              items: prevState.items.concat(newItem) 
            };
          });
         
          this._inputElement.value = "";
        }
         
        console.log(this.state.items);
           
        e.preventDefault();
      }

      deleteItem(key) {//funcion para eliminar un objeto de la lista en el cual se le haga click
        var filteredItems = this.state.items.filter(function (item) {
          return (item.key !== key);
        });
       
        this.setState({
          items: filteredItems
        });
      }


    render(){
        return (//Formato se agregar o eliminar objeto de la lista que se manda llamar en Index.JS
            <div className = "todoListMain">
                <div className = "header">
                    <form onSubmit = {this.addItem}>
                    <div className="form-inline">
                        <input className="form-control mr-1" ref ={(a) => this._inputElement = a} placeholder = " Ingrese una Tarea">
                       </input>
                        <button className="btn btn-primary" type = " submit "> Agregar </button>
                    </div>
                    </form>
                    <TodoItems entries = {this.state.items}
                               delete={this.deleteItem}/>
                </div>
            </div>
        );
    }
}

export default TodoList;