import React, { Component } from "react";
import FlipMove from "react-flip-move";

class TodoItems extends Component { 
    constructor(props){
        super(props);

        this.createTasks = this.createTasks.bind(this);
    }

    delete(key) {
        this.props.delete(key);
      }

    createTasks(item){//aqui se van cargando los objetos de la lista y al hacerles click se eliminan
        return <li onClick={() => this.delete(item.key)} key={item.key}>{item.text}</li>
    }
    render(){
        var todoEntries = this.props.entries;
        var listItems = todoEntries.map(this.createTasks);

        return (//aqui se genera una lista de manera de pila, de las tareas que se van agregando
            <ul className="theList">
                <FlipMove duration={250} easing="ease-out">
                    {listItems}                       
                </FlipMove>
            </ul>
        );
    }
};

export default TodoItems;