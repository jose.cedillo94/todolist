import 'bootstrap/dist/css/bootstrap.css';
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import TodoList from "./TodoList";


var destination = document.querySelector("#container");

ReactDOM.render(
    <div>
        {/* <p> ¡Hola mi nombre es Jose Cedillo Leal aspirante a Beliveo! </p> */}
        <TodoList/>
    </div>,
    destination
);